#pragma once
#include "game.h"
class Game;

const int FPS = 60;
const int DELAY_TIME = 1000.0f / FPS;

int main(int argc, char* args[])
{
	Uint32 frameStart, frameTime;

	std::cerr << "Game init attempt\n";

	if (TheGame::Instance()->init("SDL setup", 100, 100, 640, 480, false))
	{
		std::cerr << "Game init success\n";
		while (TheGame::Instance()->runnning())
		{
			frameStart = SDL_GetTicks();

			TheGame::Instance()->handleEvents();
			TheGame::Instance()->update();
			TheGame::Instance()->render();

			frameTime = SDL_GetTicks() - frameStart;

			//delay the frame so the frame rate is constant
			if (frameTime < DELAY_TIME)
			{
				SDL_Delay((int)(DELAY_TIME - frameTime));
			}
		}
	}
	else
	{
		std::cerr << "Game init failure - " << SDL_GetError() << "\n";
		return -1;
	}
	
	std::cerr << "Game closing...\n";
	TheGame::Instance()->clean();

	return 0;
}

