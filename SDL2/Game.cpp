#include "Game.h"
#include "InputHandler.h"
#include "PlayState.h"
#include "MenuState.h"

Game* Game::s_pInstance = 0;

Game::Game()
{
}


Game::~Game()
{
}

Game * Game::Instance()
{
	if (s_pInstance == 0)
	{
		s_pInstance = new Game();
		return s_pInstance;
	}
	return s_pInstance;
}

bool Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen)
{
	//attempt to initialise SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		//init the joysticks
		TheInputHandler::Instance()->initialiseJoysticks();
		//set mouse to relative
		SDL_SetRelativeMouseMode(SDL_bool::SDL_TRUE);

		//probably don't need this
		//SDL_Init(IMG_INIT_PNG);

		int flags = false;

		if (fullscreen)
		{
			flags = SDL_WINDOW_FULLSCREEN;
		}

		std::cerr << "SDL init success\n";

		//init the gamestatemachine
		m_pGameStateMachine = new GameStateMachine();
		m_pGameStateMachine->changeState(new MenuState());

		//init the window
		m_pWindow = SDL_CreateWindow(title, xpos, ypos, width, height, flags);
		if (m_pWindow != 0)
		{
			std::cerr << "window creation success\n";
			m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);

			if (m_pRenderer != 0)
			{
				std::cerr << "Renderer creation success\n";

				SDL_SetRenderDrawColor(m_pRenderer, 255, 255, 255, 255);

				//load the image
				if (!TheTextureManager::Instance()->load("assets/mm7_walk_armcannon-alpha.png", "animate", m_pRenderer))
				{
					return false;
				}

				//push back into the vector
				m_gameObjects.push_back(new Player(new LoaderParams(0, 0, 48, 38, "animate")));
				//m_gameObjects.push_back(new Enemy(new LoaderParams(300, 300, 48, 38, "animate")));		
			}
			else
			{
				std::cerr << "Renderer init fail\n";
				return false;
			}
		}
		else
		{
			std::cerr << "Window init fail\n";
			return false;
		}

	}
	else
	{
		std::cerr << "SDL init fail\n";
		return false;
	}

	//everything initialised successfully
	std::cerr << "init success\n";

	//start the main loop
	m_bRunning = true;

	return true;
}

void Game::draw()
{
	/*for (auto iter = m_gameObjects.begin(); iter < m_gameObjects.end(); iter++)
	{
		(*iter)->draw(m_pRenderer);
	}*/
}

void Game::render()
{
	//clear the renderer to the draw color
	SDL_RenderClear(m_pRenderer);

	/*TheTextureManager::Instance()->draw("animate", 0, 0, 48, 38, m_pRenderer);

	TheTextureManager::Instance()->drawFrame("animate", 100, 100, 48, 38, 1, m_currentFrame, m_pRenderer);*/


	//this will break the code until I actually draw the game objects again
	m_pGameStateMachine->render();

	
	//put this loop back if the FSM is no good
	for (auto iter = m_gameObjects.begin(); iter < m_gameObjects.end(); iter++)
	{
		/*(*iter)->draw(m_pRenderer);*/
		(*iter)->draw();
	}
	



	/*m_go.draw(m_pRenderer);
	m_player.draw(m_pRenderer);*/

	//draw to the screen
	SDL_RenderPresent(m_pRenderer);
}

void Game::update()
{
	/*m_currentFrame = int(((SDL_GetTicks() / 100) % 10));
	m_go.update();
	m_player.update();*/

	m_pGameStateMachine->update();

	//put this loop back if the FSM is no good
	for (auto iter = m_gameObjects.begin(); iter < m_gameObjects.end(); iter++)
	{
		(*iter)->update();
	}
}

void Game::handleEvents()
{
	TheInputHandler::Instance()->update();

	//probably a temporary move
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RETURN))
	{
		m_pGameStateMachine->changeState(new PlayState());
	}
}

void Game::clean()
{
	std::cerr << "Cleaning game\n";
	TheInputHandler::Instance()->clean();
	SDL_DestroyWindow(m_pWindow);
	SDL_DestroyRenderer(m_pRenderer);
	SDL_Quit();
}

bool Game::runnning()
{
	return m_bRunning;
}

void Game::quit()
{
	//set m_bRunning to false
	m_bRunning = false;
}
