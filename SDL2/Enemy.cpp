#include "Enemy.h"


Enemy::Enemy(const LoaderParams * pParams) : SDLGameObject(pParams)
{
}

Enemy::~Enemy()
{
}

void Enemy::draw()
{
	SDLGameObject::draw();
}

void Enemy::update()
{
	//m_position.setX(m_position.getX() + 1);
	//m_position.setY(m_position.getY() + 1);
	std::cout << "X: " << m_position.getX() << std::endl;
	std::cout << "Y: " << m_position.getY() << std::endl;
	m_currentFrame = int(((SDL_GetTicks() / 100) % 10));
}

void Enemy::clean()
{
	//Commenting the clean call, will probably throw errors or something
	//GameObject::clean();
}
