#include "Player.h"
#include "InputHandler.h"



Player::Player(const LoaderParams * pParams) : SDLGameObject(pParams)
{
	/*m_velocity.setX(0);
	m_velocity.setY(0);*/
	m_acceleration.setX(0.0);
	m_acceleration.setY(0.0);
	
}

Player::~Player()
{
}

void Player::draw()
{
	SDLGameObject::draw();
}

void Player::update()
{
	m_velocity.setX(0);
	m_velocity.setY(0);

	/*std::cout << "X: " << m_position.getX() << std::endl;
	std::cout << "Y: " << m_position.getY() << std::endl;*/

	handleInput();

	/////////////////
	///MOUSE MOVE STUFF
	//Vector2D* vec = TheInputHandler::Instance()->getMousePosition();
	////m_velocity = (*vec - m_position) / 100;
	//m_velocity = (*vec - m_position);
	///////////////////


	if (moving)
	{
		//set the animation
		m_currentFrame = int(((SDL_GetTicks() / frameRate) % 10));
	}
	else
	{
		//set a frame as static
		m_currentFrame = 1;
	}
	

	//m_acceleration.setX(0.0);

	SDLGameObject::update();
}

void Player::clean()
{
	//expect this to do bad things....

	//GameObject::clean();
}

void Player::handleInput()
{
	//joystick input
	if (TheInputHandler::Instance()->joysticksInitialised())
	{
		//**********
		//AXIS
		//**********

		//left stick x-axis
		if (TheInputHandler::Instance()->xvalue(0, 1) > 0 ||
			TheInputHandler::Instance()->xvalue(0, 1) < 0)
		{
			moving = true;
			m_velocity.setX(1 * TheInputHandler::Instance()->xvalue(0, 1));
			m_acceleration.setX((TheInputHandler::Instance()->xvalue(0, 1) > 0) ? m_acceleration.getX() + 0.1 : m_acceleration.getX() + -0.1);
			/*if (m_currentFrame == 1 && frameRate > 50)
			{
				frameRate -= 15;
			}*/
			m_currentFrame = int(((SDL_GetTicks() / frameRate) % 10));
			std::cout << "Left Stick X-axis.\n";
		}
		else
		{
			moving = false;
			m_acceleration.setX(0.0);
			frameRate = 100;
		}
		//left stick y-axis
		if (TheInputHandler::Instance()->yvalue(0, 1) > 0 ||
			TheInputHandler::Instance()->yvalue(0, 1) < 0)
		{
			moving = true;
			m_velocity.setY(1 * TheInputHandler::Instance()->yvalue(0, 1));
			m_acceleration.setY((TheInputHandler::Instance()->yvalue(0, 1) > 0) ? m_acceleration.getY() + 0.1 : m_acceleration.getY() + -0.1);
			/*if (m_currentFrame == 1 && frameRate > 50)
			{
				frameRate -= 15;
			}*/
			std::cout << "Left Stick Y-axis.\n";

		}
		else
		{
			moving = false;
			m_acceleration.setY(0.0);
			frameRate = 100;
		}

		//right stick x-axis
		if (TheInputHandler::Instance()->xvalue(0, 2) > 0 ||
			TheInputHandler::Instance()->xvalue(0, 2) < 0)
		{
			m_velocity.setX(1 * TheInputHandler::Instance()->xvalue(0, 2));
		}
		//right stick y-axis
		if (TheInputHandler::Instance()->yvalue(0, 2) > 0 ||
			TheInputHandler::Instance()->yvalue(0, 2) < 0)
		{
			m_velocity.setY(1 * TheInputHandler::Instance()->yvalue(0, 2));
		}

		//*************************
		//BUTTONS
		//*************************

		//A Button
		if (TheInputHandler::Instance()->getButtonState(0, 0))
		{
			m_velocity.setX(1);
			
		}
		//B Button
		if (TheInputHandler::Instance()->getButtonState(0, 1))
		{
			m_velocity.setX(1);
			std::cout << "B pressed.\n";
		}
		//X Button
		if (TheInputHandler::Instance()->getButtonState(0, 2))
		{
			m_velocity.setX(1);
			std::cout << "X pressed.\n";
		}
		//Y Button
		if (TheInputHandler::Instance()->getButtonState(0, 3))
		{
			m_velocity.setX(1);
			std::cout << "Y pressed.\n";
		}
		//Left Shoulder Button
		if (TheInputHandler::Instance()->getButtonState(0, 4))
		{
			m_velocity.setX(1);
			std::cout << "LB pressed.\n";
		}
		//Right Shoulder Button
		if (TheInputHandler::Instance()->getButtonState(0, 5))
		{
			m_velocity.setX(1);
			std::cout << "RB pressed.\n";
		}
		//Select Button
		if (TheInputHandler::Instance()->getButtonState(0, 6))
		{
			m_velocity.setX(1);
			std::cout << "Select pressed.\n";
		}
		//Start Button
		if (TheInputHandler::Instance()->getButtonState(0, 7))
		{
			m_velocity.setX(1);
			std::cout << "Start pressed.\n";
			TheGame::Instance()->quit();
		}
		//LStick Button
		if (TheInputHandler::Instance()->getButtonState(0, 8))
		{
			m_velocity.setX(1);
			std::cout << "LS pressed.\n";
		}
		//RStick Button
		if (TheInputHandler::Instance()->getButtonState(0, 9))
		{
			m_velocity.setX(1);
			std::cout << "RS pressed.\n";
		}
		//1 Button
		if (TheInputHandler::Instance()->getButtonState(0, 10))
		{
			m_velocity.setX(1);
			std::cout << "1 pressed.\n";
		}
		//RStick Button
		if (TheInputHandler::Instance()->getButtonState(0, 11))
		{
			m_velocity.setX(1);
			std::cout << "2 pressed.\n";
		}
		//1 Button
		if (TheInputHandler::Instance()->getButtonState(0, 12))
		{
			m_velocity.setX(1);
			std::cout << "3 pressed.\n";
		}
		//RStick Button
		if (TheInputHandler::Instance()->getButtonState(0, 13))
		{
			m_velocity.setX(1);
			std::cout << "4 pressed.\n";
		}
	}
	//mouse input
	if (TheInputHandler::Instance()->getMouseButtonState(LEFT))
	{
		m_velocity.setX(1);
		
		
	}
	if (TheInputHandler::Instance()->getMouseButtonState(MIDDLE))
	{
		m_velocity.setX(1);
	}
	if (TheInputHandler::Instance()->getMouseButtonState(RIGHT))
	{
		m_velocity.setY(1);
	}
	//keyboard input
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT))
	{
		m_velocity.setX(2);
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT))
	{
		m_velocity.setX(-2);
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP))
	{
		m_velocity.setY(-2);
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN))
	{
		m_velocity.setY(2);
	}
	if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE))
	{
		//release the mouse...just for fun
		SDL_SetRelativeMouseMode(SDL_bool::SDL_FALSE);
	}
	
	
}
