#include "MenuButton.h"
#include "InputHandler.h"


MenuButton::MenuButton(const LoaderParams * pParams) : SDLGameObject(pParams)
{
	m_currentFrame = MOUSE_OUT; //start at frame 0
}

void MenuButton::draw()
{
	SDLGameObject::draw(); //use the base class draw function
}

void MenuButton::update()
{
	//get the co-ords of the mouse pointer
	Vector2D* pMousePos = TheInputHandler::Instance()->getMousePosition();

	//check whether the mouse is over the button or not
	if (pMousePos->getX() < (m_position.getX() + m_width)
		&& pMousePos->getX() > m_position.getX()
		&& pMousePos->getY() < (m_position.getY() + m_height)
		&& pMousePos->getY() > m_position.getY())
	{
		//mouse must be over the button, set the hover sprite
		m_currentFrame = MOUSE_OVER; //frame 1

		if (TheInputHandler::Instance()->getMouseButtonState(LEFT))
		{
			m_currentFrame = CLICKED; //frame 2
		}
	}
	else
	{
		m_currentFrame = MOUSE_OUT; //frame 0
	}
}

void MenuButton::clean()
{
	SDLGameObject::clean();
}

MenuButton::~MenuButton()
{
}
