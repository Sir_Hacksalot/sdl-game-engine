#include "GameStateMachine.h"

/*
PROGRAM WILL TRY TO ACCESS DELETED STATES, SEE WORKAROUND ON REDDIT
*/



GameStateMachine::GameStateMachine()
{
}


GameStateMachine::~GameStateMachine()
{
}

void GameStateMachine::pushState(GameState * pState)
{
	//push the passed-in pstate param into the gamestates array and then call its onEnter function (from GameState.h)
	m_gameStates.push_back(pState);
	m_gameStates.back()->onEnter();
}

void GameStateMachine::changeState(GameState * pState)
{
	//this function is expected to have problems

	//check if there are any states in the array
	if (!m_gameStates.empty())
	{
		if (m_gameStates.back()->getStateID() == pState->getStateID())
		{
			//if the stateID is the same as the current one, do nothing
			return; //do nothing and exit the function
		}

		if (m_gameStates.back()->onExit())
		{
			//StateID is not the same, so remove the current state 
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}

	//pushback the new state
	m_gameStates.push_back(pState);

	//initialise it
	m_gameStates.back()->onEnter();
}

void GameStateMachine::popState()
{
	//check if there are any available states to remove.
	//if there are, call the onExit function of the current state, then remove it
	if (!m_gameStates.empty())
	{
		if (m_gameStates.back()->onExit())
		{
			delete m_gameStates.back();
			m_gameStates.pop_back();
		}
	}
}

void GameStateMachine::update()
{
	//check for any states
	if (!m_gameStates.empty())
	{
		//update the state
		m_gameStates.back()->update();
	}
}

void GameStateMachine::render()
{
	//check for any states
	if (!m_gameStates.empty())
	{
		//render the state
		m_gameStates.back()->render();
	}
}

