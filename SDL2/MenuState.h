#pragma once
#include "GameState.h"
#include "GameObject.h"
#include <vector>

class MenuState :
	public GameState
{
public:
	MenuState();
	~MenuState();

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	std::vector<GameObject*> m_gameObjects;

	virtual std::string getStateID() const { return s_menuID; }

private:
	static const std::string s_menuID;

};

