#pragma once

#include <iostream>
#include <vector>
#include <SDL.h>
#include <SDL_image.h>
#include "TextureManager.h"
#include "Player.h"
#include "Enemy.h"
#include "GameStateMachine.h"



class Game

{
public:

	~Game();

	static Game* Instance();

	SDL_Renderer* getRenderer() const { return m_pRenderer; }

	bool init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);
	void draw();
	void render();
	void update();
	void handleEvents();
	void clean();
	bool runnning();

	//adding this quit function because I think I have to
	void quit();

private:

	Game();

	static Game* s_pInstance;

	GameStateMachine* m_pGameStateMachine;

	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;

	std::vector<GameObject*> m_gameObjects;

	int m_currentFrame;

	bool m_bRunning;
};
typedef Game TheGame;

 

