#include "Vector2D.h"



Vector2D::Vector2D()
{
	
}


Vector2D::~Vector2D()
{
}

float Vector2D::length()
{
	//length of a vector
	//lengthofvector(x,y) = sqrt(x * x + y * y)
	return sqrt(m_x * m_x + m_y * m_y);
}

Vector2D Vector2D::operator+(const Vector2D & v2) const
{
	//enable easy addition of vectors
	return Vector2D(m_x + v2.m_x, m_y + v2.m_y);
}

Vector2D Vector2D::operator*(float scalar)
{
	//multiply by scalar number
	return Vector2D(m_x * scalar, m_y * scalar);
}

Vector2D Vector2D::operator*=(float scalar)
{
	//multiply by scalar number
	m_x *= scalar;
	m_y *= scalar;

	return *this;
}

Vector2D Vector2D::operator-(const Vector2D & v2) const
{
	//subtraction of vectors
	return Vector2D(m_x - v2.m_x, m_y - v2.m_y);
}

Vector2D Vector2D::operator/(float scalar)
{
	//divide by a scalar number
	return Vector2D(m_x / scalar, m_y / scalar);
}

Vector2D & Vector2D::operator/=(float scalar)
{
	//divide by a scalar number
	m_x /= scalar;
	m_y /= scalar;

	return *this;
}

void Vector2D::normalize()
{
	//Normalizing a vector makes its length equal to 1. Vectors with a length of 1 are known as unit vectors.
	//These are useful to represent a direction, such as the facing direction of an object.
	//To normalize a vector we multiply it by the inverse of its length.

	float l = length();
	if (l > 0)	//we never want to attempt to divide by zero
	{
		(*this) *= 1 / l;
	}
}

Vector2D & operator+=(Vector2D & v1, const Vector2D & v2)
{
	//enable the += using vectors
	v1.m_x += v2.m_x;
	v1.m_y += v2.m_y;

	return v1;
}

Vector2D & operator-=(Vector2D & v1, const Vector2D & v2)
{
	// subtraction of vectors
	v1.m_x -= v2.m_x;
	v1.m_y -= v2.m_y;

	return v1;
}
