#pragma once
#include "GameState.h"
#include <vector>

class GameStateMachine
{
public:
	GameStateMachine();
	~GameStateMachine();

	//add state without removing previous
	void pushState(GameState* pState);

	//remove previous state before adding another
	void changeState(GameState* pState);

	//remove currently used state without adding another
	void popState();

	void update();
	void render();

private:
	std::vector<GameState*> m_gameStates;



};

