#pragma once
//#include <SDL.h>
//#include <vector>
#include "Game.h"
#include "Vector2D.h"

enum mouse_buttons
{
	LEFT = 0,
	MIDDLE = 1,
	RIGHT = 2
};




class InputHandler
{
public:

	static InputHandler* Instance();

	void update();
	void clean();

	void initialiseJoysticks();
	bool joysticksInitialised();

	bool getButtonState(int joy, int buttonNumber);

	int xvalue(int joy, int stick);
	int yvalue(int joy, int stick);

	//mouse related input
	bool getMouseButtonState(int buttonNumber);
	Vector2D* getMousePosition();

	//keyboard input
	bool isKeyDown(SDL_Scancode key);
	

private:

	InputHandler();
	~InputHandler();

	//button states for gamepads
	std::vector<std::vector <bool>> m_buttonStates;
	//button states for mouse
	std::vector<bool> m_mouseButtonStates;

	//mouse
	Vector2D* m_mousePosition;

	//keyboard
	Uint8* m_keystates;

	//joysticks
	const int m_joystickDeadZone = 10000;
	std::vector<SDL_Joystick*> m_joysticks;
	std::vector<std::pair<Vector2D*, Vector2D*>> m_joystickValues;
	bool m_bJoysticksInitialised;

	//functions to handle different event types

	//handle keyboard events
	void onKeyDown();
	void onKeyUp();

	//handle mouse events
	void onMouseMove(SDL_Event &event);
	void onMouseButtonDown(SDL_Event &event);
	void onMouseButtonUp(SDL_Event &event);

	//handle joystick events
	void onJoystickAxisMove(SDL_Event &event);
	void onJoystickButtonDown(SDL_Event &event);
	void onJoystickButtonUp(SDL_Event &event);


	static InputHandler* s_pInstance;
};
typedef InputHandler TheInputHandler;
